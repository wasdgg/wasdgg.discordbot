const gulp = require("gulp");
const eslint = require("gulp-eslint");
const nodemon = require("gulp-nodemon");

const paths = {
    scripts: ["src/**/*.js", "!node_modules/**"]
};

gulp.task("watch", ["lint"], () => {
    nodemon({
        env: { NODE_ENV: "development" },
        ext: "js",
        script: "./src/app.js",
        tasks: ["lint"]
    });
});

gulp.task("lint", () => {
    return gulp.src(paths.scripts)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failOnError());
});