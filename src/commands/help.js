const fs = require("fs");

module.exports = function help(bot, message) {
    fs.readFile("./static/help", "utf-8", (err, data) => {
        if (err) {
            bot._bot.sendMessage(message.author, "An error occured reading the help file. Please try again or report that issue");
        } else {
            bot._bot.sendMessage(message.author, data);
        }
        
    });
};