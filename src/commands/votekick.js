"use strict";

module.exports = (bot, message) => {
    if (!bot._votekickInProgress) {
        bot._votekickInProgress = true;
        let message_send = "";
        
        if (message.mentions.length >= 2 || message.everyoneMentioned) {
            message_send = `Slow your horses cowboy, one at a time ${message.author.mention()}`;
        } else if (message.mentions.length === 0) {
            message_send = `Votekick who? ${message.author.mention()}`;
        } else if (message.isMentioned(bot._bot.user)) {
            message_send = `Nope ${message.author.mention()}`;
        } else {
            message_send = `Votekick initiated to kick ${message.mentions[0].mention()}. Cast your vote with !votekick yes/no. Time limit: 20 seconds`;
            
            setTimeout(() => {
                bot._votekickInProgress = false;
                bot._bot.sendMessage(message.channel, "Votekick failed - not enough votes");
            }, 20000);
        }
        
        bot._bot.sendMessage(message.channel, message_send);
    }
};