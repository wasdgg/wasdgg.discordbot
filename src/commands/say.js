"use strict";
const emoji = require("node-emoji");
const config = require("../config");

module.exports = function say(bot, message) {
    var server = bot._bot.servers.get("id", config.ids.WASD_SERVER_ID);
    let channel = message.channel;
    let message_send = "";
    const split = message.content.split(" ");
        
    if (message.author.id !== config.ids.BONTSCHO_ID) {
        message_send = emoji.emojify("Nope :middle_finger:");
    } else if (split.length < 3) {
        message_send = "Invalid format";
    } else {
        const channel_name = split[1];
        channel = server.channels.get("name", channel_name);
        if (channel === null) {
            message_send =  "Unknown channel";
        } else {
            message_send = split.slice(2).join(" ");
        }
    }
    
    bot._bot.sendMessage(channel, message_send);
};