var node_google = require("google");

node_google.lang = "en";
node_google.tld = "com";

module.exports = function google(bot, message) {
    const index = message.content.indexOf(" ");
    if (index === -1) {
        bot._bot.sendMessage(message.channel, "Google what?");
        return;
    }
    
    bot._bot.startTyping(message.author, () => {
        setTimeout(() => {
            bot._bot.stopTyping(message.author);
        },2000);
        const query = message.content.slice(index);
        node_google(query, (err, res) => {
            if (err) {
                bot._bot.sendMessage(message.author, "Error while searching");
            } else {
                const link = res.links[0] || { title: "Nothing found", href: "", description: "" };
                bot._bot.sendMessage(message.author, `Top Result: ${link.title}\n${link.href}\n${link.description}`);
            }
        });
    });
};