"use strict";
const emoji = require("node-emoji");
const randomColor = require("randomcolor");

module.exports = function colorparty(bot, message) {
    var can_manage_roles = false;
    
    // loop through server roles
    const roles = bot.roles();
    var old_colors = {};
    var partyInterval = null;
    
    for (var role of roles) {
        if (role.hasPermission("manageRoles") && message.author.hasRole(role)) {
            can_manage_roles = true;
        }
        // store old role colors
        old_colors[role.id] = role.color;
    }
    
    if (can_manage_roles === true && bot._colorPartyInProgress === false) {
        bot._colorPartyInProgress = true;
        bot._bot.sendMessage(message.channel, emoji.emojify("Lets get this party started :tada:"));
        
        partyInterval = setInterval(() => {
            for (var r of roles) {
                bot._bot.updateRole(r, {
                    color: parseInt(randomColor().slice(1), 16)
                });
            }
        },1000);
        
        setTimeout(() => {
            clearInterval(partyInterval);
            partyInterval = null;
            bot._colorPartyInProgress = false;
            for (var r of roles) {
                bot._bot.updateRole(r, {
                    color: old_colors[r.id]
                });
            }
        },10000);
    } else {
        bot._bot.sendMessage(message.channel, emoji.emojify("Nope :middle_finger:"));
    }
};