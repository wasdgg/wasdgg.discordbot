"use strict";

const _ = require("lodash");

module.exports = function random(bot, message) {
    const params = message.content.split(" ");
    let r = 0;

    if (params.length === 2) {
        r = _.random(parseInt(params[1], 10));
    } else {
        r = _.random(1, true);
    }

    bot._bot.sendMessage(message.channel, `Result: ${r} ${message.author.mention()}`);
};