const _ = require("lodash");

module.exports = function ping(bot, message) {
    const excuses = [
        "i'm not in the mood for that",
        "not now, i have a headache",
        "i'm not your slave",
        "go pong yourself"
    ];
    if (_.random(1, true) > 0.9) {
        bot._bot.sendMessage(message.channel, _.sample(excuses));
    } else {
        bot._bot.sendMessage(message.channel, "pong");
    }
};