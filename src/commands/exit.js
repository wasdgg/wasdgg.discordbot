"use strict";
const emoji = require("node-emoji");
const config = require("../config");

module.exports = function exit(bot, message) {
    if (message.author.id === config.ids.BONTSCHO_ID) {
        bot._bot.sendMessage(message.channel, "bye bye");
        bot._bot.logout();
    } else {
        bot._bot.sendMessage(message.channel, emoji.emojify("Nope :middle_finger:"));
    }
};