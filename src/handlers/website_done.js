const emoji = require("node-emoji");

module.exports = function websiteDone(bot, message) {
    if (message.isMentioned(bot._bot.user) && message.content.indexOf("when is website done?") !== -1) {
        bot._bot.sendMessage(message.channel, emoji.emojify("soon:tm: ") + message.author.mention());
    }
};