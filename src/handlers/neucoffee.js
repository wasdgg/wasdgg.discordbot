const emoji = require("node-emoji");

module.exports = function neucoffee(bot, message) {
    if (message.content.indexOf(":neucoffee") !== -1) {
        bot._bot.sendMessage(message.channel,  emoji.emojify(":neutral_face: :coffee:"));
    }
};