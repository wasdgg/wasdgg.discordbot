const emoji = require("node-emoji");

module.exports = function trolleybus(bot, message) {
    if (message.content.indexOf(emoji.get("trolleybus")) !== -1) {
        bot._bot.sendMessage(message.channel, emoji.get("trolleybus"));
    }
};