const emoji = require("node-emoji");

module.exports = function love_you(bot, message) {
    if ((message.content.indexOf("love you") !== -1 || message.content.indexOf("luv u") !== -1) && message.isMentioned(bot._bot.user) && (message.content.indexOf("dont") === -1 && message.content.indexOf("don't") === -1 && message.content.indexOf("not") === -1)) {
        bot._bot.sendMessage(message.channel, emoji.emojify("i :heart: you too ") + message.author.mention());
    }
};