"use strict";
const config = require("../config");

module.exports = function everyoneMention(bot, message) {
    if (message.everyoneMentioned) {
        if (message.author.id === config.ids.MAD_ID || message.author.id === config.ids.SCANFERR_ID) {
            bot._bot.sendMessage(message.channel, "shut up " + message.author.mention());
        }
        bot._bot.sendMessage(message.author, "The @everyone mention can be very annoying to other users, please refrain from using it in the future");
    }
};