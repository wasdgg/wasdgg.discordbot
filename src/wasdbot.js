"use strict";
const config = require("./config");
const Commands = require("./commands");
const Discord = require("discord.js");
const Handlers = require("./handlers");
const moment = require("moment");

class WASDBot {
    constructor(bot_settings) {
        this._settings = bot_settings || {};
        this._bot = new Discord.Client();
        this._handleDisconnect = () => {
            console.log(`[${moment().format()}] Disconnect event received`);
            this._reconnectInterval = setInterval(() => {
                this._bot.login(this._settings.email, this._settings.password);
                console.log(`[${moment().format()}] Trying reconnect`);
            }, 60 * 1000);
        };
        this._reconnectInterval = null;
        this._votekickInProgress = false;
        this._colorPartyInProgress = false;
        
        this._prepareCommandsAndHandlers();
    };
    
    login() {
        this._bot.login(this._settings.email, this._settings.password);
        this._bot.on("disconnected", this._handleDisconnect);
    };
    
    logout() {
        this._bot.logout();
        this._bot.off("disconnected", this._handleDisconnect);
        if (this._reconnectInterval !== null) {
            clearInterval(this._reconnectInterval);
            this._reconnectInterval = null;
        }
    };
    
    roles() {
        return this.server().roles;
    };
    
    server() {
        return this._bot.servers.get("id", config.ids.WASD_SERVER_ID);
    };
    
    _prepareCommandsAndHandlers() {
        const self = this;
        
        this._bot.on("ready", () => {
            console.log(`[${moment().format()}] Ready event received`);
            if (this._reconnectInterval !== null) {
                clearInterval(this._reconnectInterval);
                this._reconnectInterval = null;
            }
        });
        
        this._bot.on("message", (message) => {
            // ignoring own message first to avoid possible endlessloops
            if (message.author.equals(this._bot.user)) {
                return;
            }
            // first char is ! for commands and are sent to command functions
            // rest is piped through message handlers
            if (message.content[0] === "!") {
                const index = message.content.indexOf(" ");
                let command = "";
                if (index === -1) {
                    command = message.content.slice(1);
                } else {
                    command = message.content.slice(1, index);
                }

                if (typeof Commands[command] === "undefined") {
                    this._bot.sendMessage(message.channel, `Unknown command "!${command}". Type !help to list all available commands ${message.author.mention()}`);
                } else {
                    Commands[command](self, message);
                }
            } else {
                for (const prop in Handlers) {
                    if (typeof Handlers[prop] === "function") {
                        Handlers[prop](self, message);
                    }
                }
            }
        });
    };
};

module.exports = WASDBot;