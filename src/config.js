"use strict";

module.exports = {
    development:{
        env: "development",
        port: 3000,
        isDev: true,
        isProd: false,
        discord_bot_settings: {
            email: process.env.WASDGG_BOT_EMAIL || "email@domain.com",
            password: process.env.WASDGG_BOT_PASSWORD || "secretpassword"
        }
    },
    production:{
        env: "production",
        isDev: false,
        isProd: true,
        port: process.env.PORT || 3000,
        discord_bot_settings: {
            email: process.env.WASDGG_BOT_EMAIL || "email@domain.com",
            password: process.env.WASDGG_BOT_PASSWORD || "secretpassword"
        }
    },
    ids: {
        WASD_SERVER_ID: "93426448068513792",
        BONTSCHO_ID: "160496062103158784",
        SCANFERR_ID: "93423581697945600",
        MAD_ID: "93426376517881856"
    }
};