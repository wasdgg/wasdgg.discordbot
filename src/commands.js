const fs = require("fs");
const path = require("path");

for (var file of fs.readdirSync(path.resolve(__dirname, "commands"))) {
    module.exports[path.basename(file, ".js")] = require(path.resolve(__dirname, "commands", file));
}