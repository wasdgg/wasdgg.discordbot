const compression = require("compression");
const config = require("./config")[process.env.NODE_ENV];
const express = require("express");
const WASDBot = require("./wasdbot");

var app = express();
app.use(compression());
app.use(express.static("public"));

app.listen(config.port);

var mybot = new WASDBot(config.discord_bot_settings);

/* debug stuff
mybot._bot.on("message", function(message) {
//    console.log(message.author.mention());
});

mybot._bot.on("ready", function() {
    //var server = mybot.servers.get("id", wasd_server_id);
    //var mad = server.members.get("username", "bontscho");
    //console.log(mad);
}); */

mybot.login();